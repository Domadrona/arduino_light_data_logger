/*
  SD card datalogger

 This example shows how to log data from three analog sensors
 to an SD card using the SD library.

 The circuit:
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 9 (for MKRZero SD: SDCARD_SS_PIN)

 created  24 Nov 2010
 modified 9 Apr 2012
 by Tom Igoe

 This example code is in the public domain.
  Led - pin 10
  Adding button, debouncing Misssing
  interrupt - Pin 2
  
 */
#include <SPI.h>
#include <SD.h>
#include <TimerOne.h>

const int chipSelect = 9;
const int analogInPin = A0;
int sensorValue = 0; 
const byte interruptPin = 2;
volatile byte state = LOW;

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

void setup() 
{
    // Define interruption pin and function call
     pinMode(interruptPin, INPUT_PULLUP);
     attachInterrupt(digitalPinToInterrupt(interruptPin), Record, CHANGE);
    // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
  }
  Serial.println("card initialized.");

  // Initialize the digital pin as an output.
  // Pin 10 LED
  pinMode(10, OUTPUT);    
  
  Timer1.initialize(15000000); // set a timer of length 100000 microseconds (or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second)
  Timer1.attachInterrupt( timerIsr ); // attach the service routine here
}
 
void loop()
{
  // Main code loop
  // TODO: Put your regular (non-ISR) logic here
//digitalWrite(10, LOW);
Serial.print("state: ");
Serial.println(state);
        if (state == LOW)
        {
          digitalWrite(10, HIGH);
        }
        else digitalWrite(10, LOW);

/*
  // make a string for assembling the data to log:
  String dataString = "";

  // read three sensors and append to the string:
  for (int analogPin = 0; analogPin < 3; analogPin++) {
    int sensor = analogRead(analogPin);
    dataString += String(sensor);
    if (analogPin < 2) {
      dataString += ",";
    }
  }
*/


}
 
/// --------------------------
/// Custom ISR Timer Routine
/// --------------------------
void timerIsr()
{
   if(state == HIGH) {
                            sensorValue = analogRead(analogInPin);
                           // dataString
                            //String dataString = "sensorValue";// String dataString = "";
                              // Toggle LED
                              digitalWrite(10, HIGH);
                          
                            // open the file. note that only one file can be open at a time,
                            // so you have to close this one before opening another.
                            File dataFile = SD.open("info.txt", FILE_WRITE);
                          
                            // if the file is available, write to it:
                            if (dataFile) {
                              dataFile.println(sensorValue);
                              dataFile.close();
                              // print to the serial port too:
                              Serial.println(sensorValue);
                            }
                            // if the file isn't open, pop up an error:
                            else {
                              Serial.println("error opening datalog.txt");
                            }
                            digitalWrite(10, LOW);
                      }
}

void Record() {
 //  lastTime = millis();

  state = !state;

}
